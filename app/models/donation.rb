class Donation < ActiveRecord::Base
  belongs_to :donor

  validates :donor_id, :amount, :presence => true
end
