class Donor < ActiveRecord::Base
  has_many :donations

  validates :name, :email, :presence => true


def total
  donations.sum(:amount)

end

end
